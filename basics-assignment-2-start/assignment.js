const task3Element = document.getElementById('task-3');

function alertFunc () {
    alert('any message');
}

function greetUser(name) {
    alert(name);
}

function concat(firstString, secondString, thirdString){
    const result = firstString + secondString + thirdString;
    alert(result);
}

task3Element.addEventListener('click', alertFunc);

alertFunc();
greetUser('mark');
concat('i ', 'am ', 'hacker');